
#include "main.h"

using namespace std;


void mouseToBoard(sf::Vector2i mousePos, int &x ,int &y);

#define BOARD_SIZE 10
#define gridX  125
#define gridY  125
#define cellSize 25
#define nBombs 20

int main()
{
	//GameConfigs
	
	sf::RenderWindow window(sf::VideoMode(500, 500), "SFML Minesweeper!",sf::Style::Titlebar | sf::Style::Close);
	window.setFramerateLimit(30);
	window.setVerticalSyncEnabled(true);
	
	Resources::loadAssets();

	BoardManager board(BOARD_SIZE,nBombs,cellSize,gridX,gridY);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		window.clear();
		//drawing board
		board.drawBoard(window);
		
		/*for (int i = 0; i < BOARD_SIZE; i++)
		{
			for (int j = 0; j < BOARD_SIZE ;j++)
			{
				window.draw(boardManager.boardDraw(i,j));
			}
		}*/
		//handling input
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) 
		{
			
			int x = -1, y = -1;
			
			mouseToBoard(sf::Mouse::getPosition(window), x, y);
			
			if (x >= 0 && x < BOARD_SIZE && y >= 0 && y < BOARD_SIZE)
			{
				cout << x << " " << y << endl;
				cout << "mouse x " << sf::Mouse::getPosition(window).x << " mouse y " << sf::Mouse::getPosition(window).x << endl;
				board.clickOnCell(x,y);
			}
		}
		
		window.display();

	}

	return 0;
}



void mouseToBoard(sf::Vector2i mousePos, int &x, int &y)
{
	
	x = floor((mousePos.x - gridX) / cellSize);

	y = floor((mousePos.y - gridY) / cellSize);
}

