#include "Board.h"
#include "Cell.h"


using namespace std;

vector<int> bombIndexes(int bombs, int spots);

BoardManager::BoardManager(int boardSize, int nBombs, int cellSize, int boardX, int boardY)
{
	int cellCount = 0;
	this->boardSize = boardSize;
	bombSpots = bombIndexes(nBombs, boardSize*boardSize);
	board.resize(boardSize);

	for (int i = 0; i < boardSize; i++)
	{
		std::vector<std::shared_ptr<Cell>> line(boardSize);
		
		for (int j = 0; j < boardSize; j++)
		{

			bool bBomb = false;

			if (nBombs > 0)
			{
				bBomb = find_if(bombSpots.begin(), bombSpots.end(), [&cellCount](int k) { return k == cellCount; }) != bombSpots.end();
				if (bBomb)nBombs--;
			}

			shared_ptr<Cell> cell = make_shared<Cell>(sf::Vector2f(i * cellSize + boardX, j * cellSize + boardY), i, j, cellSize, false, bBomb);
			cellCount++;
			line[j] = cell;
			board[i] = line;
		}
	}
}

BoardManager::~BoardManager()
{
}


void BoardManager::drawBoard(sf::RenderWindow  & window)
{
	for (int i = 0; i < boardSize; i++)
	{
		for (int j = 0; j < boardSize; j++)
		{
			window.draw(boardDraw(i, j));
		}
	}
}

void BoardManager::clickOnCell(int i, int j)
{
	if (board[i][j].get()->bBomb)
	{
		revealAllBombs();
		return;
	}

	revealCell(i, j);
}

void BoardManager::revealCell(int i, int j)
{
	
	if (board[i][j].get()->bRevealed) 
	{
		return;
	}
	board[i][j].get()->bRevealed = true;

	std::vector<std::shared_ptr<Cell>> neighbours; 
	getNeighboursFromCell(i, j, neighbours);

	int bombsNearby = 0;

	for (auto & n : neighbours) 
	{
		if(n.get()->bBomb)
			bombsNearby++;
	}
	if (bombsNearby > 0) 
	{
		board[i][j].get()->mValue = bombsNearby;
		return;
	}


	board[i][j].get()->bEmpty = true;

	for (auto & n : neighbours) 
	{
		revealCell(n.get()->x, n.get()->y);
	}
	
}

void BoardManager::revealAllBombs()
{
	for (int i = 0; i < boardSize; i++)
	{
		for (int j = 0; j < boardSize; j++)
		{
			if (board[i][j].get()->bBomb)
			{
				board[i][j].get()->bRevealed = true;
			}
		}
	}
}

void BoardManager::getNeighboursFromCell(int i, int j, std::vector<std::shared_ptr<Cell>>& neighbours)
{
	for (int x = -1; x <= 1; x++)
	{
		for (int y = -1; y <= 1; y++)
		{
			int gridX = x + i;
			int gridY = y + j;

			if (gridX == i && gridY == j) continue;

			if (gridX >= 0 && gridX < boardSize && gridY >= 0 && gridY < boardSize)
			{
				if (!board[gridX][gridY].get()->bRevealed)
					neighbours.push_back(board[gridX][gridY]);
			}
		}
	}
}

sf::RectangleShape BoardManager::boardDraw(int i, int j)
{
	if (board[i][j].get()->bRevealed)
	{
		if (board[i][j].get()->bBomb)
			board[i][j].get()->setBombTexture();
		else if (board[i][j].get()->bEmpty)
			board[i][j].get()->setEmptyTexture();
		else
			board[i][j].get()->setNumberTexture();
	}
	return board[i][j].get()->getShape();
}

/* Aux functions */
vector<int> bombIndexes(int bombs, int spots)
{
	vector<int> shuffle(spots);


	for (int i = 0; i < spots; ++i)
	{
		shuffle.push_back(i);
	}

	vector<int> pick(bombs);

	random_device rd; // obtain a random number from hardware
	mt19937 eng(rd()); // seed the generator
	uniform_int_distribution<> distr(0, spots); // define the range

	for (int i = 0; i < shuffle.size(); ++i)
	{
		int j = distr(eng);
		int temp = shuffle[i];
		shuffle[i] = shuffle[j];
		shuffle[j] = temp;
	}


	for (int i = 0; i < bombs; ++i)
	{
		pick[i] = (shuffle[i]);
	}

	return pick;
}