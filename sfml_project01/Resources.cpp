#include "Resources.h"
#include <assert.h>

const std::shared_ptr<sf::Texture>& bombInitializer()
{
	auto a = std::make_shared<sf::Texture>();
	return  a;
}

std::vector<std::shared_ptr<sf::Texture>> Resources::tNumbers;
std::shared_ptr<sf::Texture> Resources::bomb { std::shared_ptr<sf::Texture>() };



void Resources::loadAssets()
{
	loadNumbers();
	loadbomb();
}

void Resources::loadNumbers()
{

	if (!tNumbers.empty()) return;

	std::string path("Number_Blocks_01_Set_4_64x64_");
	std::string extensionFile(".png");
	for (int i = 0; i < 10; i++)
	{
		std::shared_ptr<sf::Texture> number = std::make_shared<sf::Texture>();
		number.get()->loadFromFile("../Textures/Numbers/" + path + std::to_string(i) + extensionFile);
		tNumbers.push_back(number);
		
	}
}

void Resources::loadbomb()
{
	std::string path("bomb.png");
	bomb = std::make_shared<sf::Texture>();
	bomb.get()->loadFromFile("../Textures/Bomb/" + path, sf::IntRect(0,0,64,64));
}


sf::Texture *Resources::getTextureNumber(int number)
{
	
	if (tNumbers.empty())
	{
		loadNumbers();
	}
	return tNumbers[number].get();
	
}

sf::Texture * Resources::getTextureBomb()
{
	return bomb.get();
}
