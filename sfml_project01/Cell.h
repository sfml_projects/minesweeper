#pragma once
#include "Resources.h"
#include <iostream>
#include <assert.h>


class Cell
{
public:
	Cell(sf::Vector2f _pos, int _x, int _y, int _size, bool _r, bool _b) : pos(_pos), x(_x), y(_y), size(_size), bRevealed(_r),bBomb(_b)
	{
		mCellRect = sf::RectangleShape(sf::Vector2f(size, size));
		mCellRect.setFillColor(sf::Color::White);
		mCellRect.setOutlineColor(sf::Color::Black);
		mCellRect.setOutlineThickness(2);
		mCellRect.setPosition(pos);
		this->bEmpty = false;
	}
	Cell();
	~Cell();

private:
	sf::RectangleShape mCellRect;

public:
	bool bEmpty;
	sf::Vector2f pos;
	int x;
	int y;
	int mValue;
	int size;
	bool bBomb;
	bool bRevealed;
public:
	void setNumberTexture();
	void setBombTexture();
	void setEmptyTexture();
	inline sf::RectangleShape& getShape() { return mCellRect; }
};

