#include "Cell.h"

Cell::Cell()
{
}


Cell::~Cell()
{
	std::cout << "Destroyed" << std::endl;
}

void Cell::setNumberTexture()
{
	mCellRect.setTexture(Resources::getTextureNumber(mValue));
}

void Cell::setBombTexture()
{
	mCellRect.setTexture(Resources::getTextureBomb());
}

void Cell::setEmptyTexture()
{
	mCellRect.setFillColor(sf::Color::Color(192, 192, 192, 255));
}
