#pragma once


#ifndef RESOURCES_H
#define RESOURCES_H

#include <vector>
#include <SFML/Graphics.hpp>

class Resources 
{

private:
	static std::vector<std::shared_ptr<sf::Texture>> tNumbers;
	static std::shared_ptr<sf::Texture> bomb;
	static void loadNumbers();
	static void loadbomb();

public:
	static void loadAssets();
	static sf::Texture* getTextureNumber(int number);
	static sf::Texture* getTextureBomb();
	
};



#endif