#pragma once
#include "Cell.h"
#include <iostream>
#include <random>

class BoardManager
{
public:
	BoardManager(int boardSize, int nBombs, int cellSize, int boardX, int boardY);
	~BoardManager();

public:
	int boardSize;
	std::vector<std::vector<std::shared_ptr<Cell>>> board;

public:
	void drawBoard(sf::RenderWindow & window);
	void clickOnCell(int i, int j);
	void revealCell(int i, int j);
	void revealAllBombs();
	void getNeighboursFromCell(int i, int j, std::vector<std::shared_ptr<Cell>>& neighbours);
	sf::RectangleShape boardDraw(int i, int j);

private:
	std::vector<int>bombSpots;

};

